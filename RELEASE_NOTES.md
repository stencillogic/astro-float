# Release notes

**0.6.0**

 - `expr!` macro implemented.
 - BigFloat stores information about its inexactness.
 - `FromExt` conversion trait added for BigFloat.

**0.5.0**

 - Release notes introduction.
 - Correct rounding of all arithmetic operations, math functions, and constant values.
 - Api compliance with https://rust-lang.github.io/api-guidelines/
